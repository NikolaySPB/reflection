﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Reflection
{
    class SerializerFormat 
    {    
        public void CSVIterations1000(Data data)
        {
            Console.WriteLine("Мой рефлекшн:");

            Stopwatch stopwatchSCSV = new Stopwatch();

            stopwatchSCSV.Start();

            for (int i = 0; i < 100000; i++)
            {
                data.ToCsv();
            }

            string result = data.ToCsv();

            stopwatchSCSV.Stop();

            Console.WriteLine($"Время на сериализацию: {stopwatchSCSV.ElapsedMilliseconds.ToString()}");

            Stopwatch stopwatchDCSV = new Stopwatch();

            stopwatchDCSV.Start();

            for (int i = 0; i < 100000; i++)
            {
                result.FromCsv<Data>();
            }

            var result2 = result.FromCsv<Data>();

            stopwatchDCSV.Stop();

            Console.WriteLine($"Время на десериализацию: {stopwatchDCSV.ElapsedMilliseconds.ToString()}");

        }

        public void JSONIterations1000(Data data)
        {
            Console.WriteLine("Стандартный механизм (Newtonsoft.Json):");

            Stopwatch stopwatchSJson = new Stopwatch();

            stopwatchSJson.Start();

            for (int i = 0; i < 100000; i++)
            {
                JsonConvert.SerializeObject(data);
            }

            string data1ClassJs = JsonConvert.SerializeObject(data);

            stopwatchSJson.Stop();

            Console.WriteLine($"Время на сериализацию: {stopwatchSJson.ElapsedMilliseconds.ToString()}");

            Stopwatch stopwatchDJson = new Stopwatch();

            stopwatchDJson.Start();

            for (int i = 0; i < 100000; i++)
            {
                JsonConvert.DeserializeObject(data1ClassJs);
            }

            var data2 = JsonConvert.DeserializeObject(data1ClassJs);

            stopwatchDJson.Stop();

            Console.WriteLine($"Время на десериализацию: {stopwatchDJson.ElapsedMilliseconds.ToString()}");
        }
    }
}

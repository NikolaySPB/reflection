﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Reflection
{
    public static class SerialozatorExt
    {
        public static string ToCsv(this object o)
        {
            string dataToCSV = string.Join('\n', o.GetType().GetMembers().Select(x => x.Name))
                + string.Join('\n', o.GetType().GetFields().Select(y => y.Name));

            return dataToCSV;
        }

        public static T FromCsv<T>(this string s) where T : new()
        {
            T dataFromCSV = new T();

            var props = dataFromCSV.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Instance);

            props.Select(x => x.Name + x.GetValue(s));

            return dataFromCSV;
        }
    }
}


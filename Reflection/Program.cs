﻿using System;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {            
            Data data = new Data();

            SerializerFormat serializerFormat = new SerializerFormat();

            serializerFormat.CSVIterations1000(data);

            serializerFormat.JSONIterations1000(data);

            Console.ReadLine();
        }
    }
}
